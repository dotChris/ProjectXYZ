$pathBefore = $PWD;

$dynamicLibExt = '';

if ($env:OS -eq 'Windows_NT') {
    $dynamicLibExt = '.dll';
}
else {
    $dynamicLibExt = '.so';
}

Set-Location($MyInvocation.MyCommand.Definition + '/..');

if ( (Test-Path -Path './obj' ) -eq $false) { mkdir './obj'  };
if ( (Test-Path -Path './bin' ) -eq $false) { mkdir './bin'  };

Get-ChildItem -Path './*.c' | ForEach-Object { gcc -c -o ('./obj/' + $_.BaseName + '.o') $_.FullName} ;

Get-ChildItem -Path './obj/*.o' | ForEach-Object { gcc -shared -o  ('./bin/' + $_.BaseName + '.dll') $_.FullName} ;
Get-ChildItem -Path './obj/*.o' | ForEach-Object { gcc -shared -o ('./bin/' + (Get-Item $pwd).Name + '.dll') $_.FullName} ;

Set-Location -Path $pathBefore;