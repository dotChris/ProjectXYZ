#include "strings.h"
#include <stdlib.h>

char* Reverse(char* word, int noOfChar)
{
    char* output = calloc(noOfChar, sizeof(char));
    for (int idx = 0; idx < noOfChar; idx++)
    {
        output[idx] = word[noOfChar-1-idx];
    }
    return output;
}