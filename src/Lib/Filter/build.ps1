$pathBefore = $PWD;

$dynamicLibExt = '';

if ($env:OS -eq 'Windows_NT') {
    $dynamicLibExt = '.dll';
}
else {
    $dynamicLibExt = '.so';
}

Set-Location($MyInvocation.MyCommand.Definition + '/..');

if ( (Test-Path -Path './obj' ) -eq $false) { mkdir './obj'  };
if ( (Test-Path -Path './bin' ) -eq $false) { mkdir './bin'  };

Get-ChildItem -Path './*.c' | ForEach-Object { gcc -c -o ('./obj/' + $_.BaseName + '.o') $_.FullName} ;

$cFiles = Get-ChildItem -Path './*.c' ;

for ($idx = 0; $idx -lt $cFiles.Count; $idx++) {
    [String[]] $includedHeaders = $cFiles[$idx]    | ForEach-Object {Get-Content -Path $_.FullName } `
                                        | Where-Object { $_.Contains('#include') } `
                                        | ForEach-Object {$_ -replace '#include ',''} `
                                        | ForEach-Object {$_ -replace '"',''} `
                                        | Where-Object {$_.Contains('..')};

    [String[]] $resolvedPath = $includedHeaders    | ForEach-Object {Resolve-Path ($cFiles[$idx].Directory.FullName + '/' + $_ )} ;

    $resolvedPath   | ForEach-Object {(Get-Item ($_)).Directory.FullName} `
                    | Select-Object -Unique `
                    | ForEach-Object {& ($_ + '/build.ps1') };
    $gccCommand = 'gcc -shared -o "./bin/' + $cFiles[$idx].BaseName + '.dll" ';

    $resolvedPath   | ForEach-Object { Get-Item $_ } `
                    | ForEach-Object { $gccCommand = $gccCommand + '"' + $_.Directory.FullName + '/obj/' + $_.BaseName + '.o" ' } ;
    $cFileObjFile = Resolve-Path ('./obj/' + $cFiles[$idx].BaseName + '.o');
    $gccCommand = $gccCommand +  '"' + $cFileObjFile + '"';

    Invoke-Expression $gccCommand;

}

Set-Location -Path $pathBefore;
