#include "Filter.h"
#include "../Util/math.h"

double Average(double *doubleArray, int arrayLenght)
{
    double sumOfArray = Sum(doubleArray, arrayLenght);

    double averageOfArray = sumOfArray / ( (double) arrayLenght);

    return averageOfArray;
}