$LibPath = '../../../src/Lib/Filter';

$binPath = $LibPath + '/bin';
$objPath = $LibPath + '/obj';


if ((Test-Path -Path $binPath) -eq $true) {Remove-Item -Path $binPath -Recurse};
if ((Test-Path -Path $objPath) -eq $true) {Remove-Item -Path $objPath -Recurse};

& ($LibPath + '/build.ps1');

dotnet build;

Get-ChildItem -Path $binPath | ForEach-Object {Copy-Item -Path $_.FullName -Destination ('./bin/Debug/netcoreapp2.0/' + $_.Name) } ;

dotnet test;