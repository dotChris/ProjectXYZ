using System;
using Xunit;
using System.Runtime.InteropServices;
using System.Linq;

namespace test.Filter
{
    internal static class FilterAdapter
    {
        // yeah it works! 
        [DllImport("Filter")]
        internal extern static double Average(double[] doubleArray, int arrayLength);
    }
    public class FilterTester
    {
        [Fact]
        public void Test1()
        {
            double[] arrayInput = new double[] {10.0,12.0,8.0};

            double outputValue = Filter.FilterAdapter.Average(arrayInput,arrayInput.Length);

            Assert.Equal(outputValue,arrayInput.Average());
        }
    }
}