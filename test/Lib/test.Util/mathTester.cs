using System;
using Xunit;
using System.Runtime.InteropServices;
using System.Linq;

namespace test.Util
{
    internal static class mathAdapter
    {
        [DllImport("math.dll")]
        internal static extern double Sum(double[] summandArray, int arrayLength);
    }
    public class mathTester
    {
        [Fact]
        public void Test1()
        {
            double[] inputArray = new double[]{1.0, 2.0,3.0,4.0};

            double output = mathAdapter.Sum(inputArray, inputArray.Length);

            Assert.Equal(output,inputArray.Sum());
        }
    }
}
